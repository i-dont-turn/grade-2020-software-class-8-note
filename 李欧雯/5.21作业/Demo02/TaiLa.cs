﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo02
{
    class TaiLa:Hero
    {
        public TaiLa():base(heroname, hintroduce, attack, defense, speed, skills, username)
        {
            Hero.Print();
        }

        public static void PrintTaiLa()
        {
            Console.WriteLine("请输入用户名称：");
            string name = Console.ReadLine();
            Hero hero = new Hero("泰拉","泰拉是为复仇而来的勇者。她挥舞法杖，将愤怒转化为强大的元素魔法和攻击力，因此战无不胜。",90,75,60,"1.巨浪冲击 2.元素突击 3.复仇杀戮",name);
            Hero.Print();
        }
    }
}
