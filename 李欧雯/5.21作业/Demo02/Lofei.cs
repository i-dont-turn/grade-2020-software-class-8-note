﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo02
{
    class Lofei:Hero
    {
        //public Lofei():base(heroname, hintroduce, attack, defense, speed, skills, username)
        //{
        //    Hero.Print();
        //}

        public Lofei() : base(heroname, hintroduce, attack, defense, speed, skills, username)
        {
            Hero.Print();
        }

        public static void PrintLofei()
        {
            Console.WriteLine("请输入用户名称：");
            string name = Console.ReadLine();
            Hero hero = new Hero("洛菲", "洛菲是一名攻击迅猛且擅长传送魔法的时空旅行者，喜欢用她的幻象伙伴迷惑，吸引并摧毁敌人", 75, 45, 85, "1.能量精灵 2.暗影传送 3.时空迸裂", name);
            Hero.Print();
        }
    }
}
