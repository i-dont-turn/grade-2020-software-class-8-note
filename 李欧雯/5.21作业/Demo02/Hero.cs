﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo02
{
    class Hero
    {
        protected static string heroname;
        protected static string hintroduce;
        protected static int attack;
        protected static int defense;
        protected static int speed;
        protected static string skills;
        protected static string username;

        public string Heroname { get => heroname; set => heroname = value; }
        public string Hintroduce { get => hintroduce; set => hintroduce = value; }
        public int Attack { get => attack; set => attack = value; }
        public int Defense { get => defense; set => defense = value; }
        public int Speed { get => speed; set => speed = value; }
        public string Skills { get => skills; set => skills = value; }
        public string Username { get => username; set => username = value; }


        public Hero(string heroname, string hintroduce, int attack, int defense, int speed, string skills, string username)
        {
            this.Heroname = heroname;
            this.Hintroduce = hintroduce;
            this.Attack = attack;
            this.Defense = defense;
            this.Speed = speed;
            this.Skills = skills;
            this.Username = username;
        }

        public static void Print()
        {
            Console.WriteLine("英雄名称："+ heroname);
            Console.WriteLine("英雄介绍：" + hintroduce);
            Console.WriteLine("攻击力：" + attack);
            Console.WriteLine("防御力：" + defense);
            Console.WriteLine("速度：" + speed);
            Console.WriteLine("英雄技能：" + skills);
            Console.WriteLine("用户名称：" + username);
        }
    }
}
