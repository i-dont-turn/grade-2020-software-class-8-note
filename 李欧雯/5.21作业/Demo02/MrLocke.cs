﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo02
{
    class MrLocke : Hero
    {
       public MrLocke():base(heroname,hintroduce,attack,defense,speed,skills,username)
        {
            Hero.Print();
        }

        public static void PrintMrLocke()
        {
            Console.WriteLine("请输入用户名称：");
            string name = Console.ReadLine();
            Hero hero = new Hero("埃洛克", "埃洛克是一位来自末日边境的勇士。他是圣约英雄中名副其实的拳术好手。他用毁灭性的符文魔法和无情的拳术攻击消灭敌人",100,70,50,"1，碎石击打 2，烈焰锚钩 3，战斗咆哮",name);
            Hero.Print();
        }
    }
}
