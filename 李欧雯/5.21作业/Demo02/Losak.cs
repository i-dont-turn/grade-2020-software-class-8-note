﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo02
{
    class Losak:Hero
    {
        public Losak():base(heroname, hintroduce, attack, defense, speed, skills, username)
        {
            Hero.Print();
        }
        public static void LosakPrint()
        {
            Console.WriteLine("请输入用户名称：");
            string name = Console.ReadLine();
            Hero hero = new Hero("卢卡斯","卢卡斯是一名彬彬有礼的剑客，能控制源质能量。他一手持剑战斗，另一手辅助攻击",80,50,75,"1.减速陷阱 2.能量浪潮 3.旋风剑舞",name);
            Hero.Print();
        }
    }
}
