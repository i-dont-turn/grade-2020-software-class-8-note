﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Teacher:Person
    {
        //编号（Id）、姓名（Name）,性别（Sex）、身份证号（Cardid）、联系方式（Tel）、职称（Title）、工资号（Wageno）
        //private int id;
        //private string name;
        //private string sex;
        //private string cardid;
        //private string tel;
        private string title;
        private string wageno;

        //public int Id { get => id; set => id = value; }
        //public string Name { get => name; set => name = value; }
        //public string Sex { get => sex; set => sex = value; }
        //public string Cardid { get => cardid; set => cardid = value; }
        //public string Tel { get => tel; set => tel = value; }
        public string Title { get => title; set => title = value; }
        public string Wageno { get => wageno; set => wageno = value; }

        public Teacher()
        {

        }
        public new void Print()
        {
            base.Print();
        }
        public Teacher(int id, string name, string sex, string cardid, string tel, string title, string wageno):base(id,name,sex,cardid,tel)
        {
            Console.WriteLine("职称:" + title);
            Console.WriteLine("工资号:" + wageno);
        }

      

        //public void Print()
        //{
        //    //Console.WriteLine("编号:"+Id);
        //    //Console.WriteLine("姓名:" + Name);
        //    //Console.WriteLine("性别:" + Sex);
        //    //Console.WriteLine("身份证:" + Cardid);
        //    //Console.WriteLine("电话:" + Tel);
        //    Console.WriteLine("职称:" + Title);
        //    Console.WriteLine("工资号:" + Wageno);
        //}
    }
}
