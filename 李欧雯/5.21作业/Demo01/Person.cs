﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    /// <summary>
    /// 父类
    /// </summary>
    class Person
    {
        private int id;
        private string name;
        private string sex;
        private string cardid;
        private string tel;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Sex { get => sex; set => sex = value; }
        public string Cardid { get => cardid; set => cardid = value; }
        public string Tel { get => tel; set => tel = value; }


        public Person()
        {

        }
        public Person(int id,string name,string sex,string cardid,string tel) 
        {
            this.id = id;
            this.name = name;
            this.sex = sex;
            this.cardid = cardid;
            this.tel = tel;
        }
        public void Print()
        {
            Console.WriteLine("编号:" + id);
            Console.WriteLine("姓名:" + name);
            Console.WriteLine("性别:" + sex);
            Console.WriteLine("身份证:" + cardid);
            Console.WriteLine("电话:" + tel);
        }
    }
}
