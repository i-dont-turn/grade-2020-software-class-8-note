﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Student:Person
    {
        //编号（Id）、姓名（Name）、性别（Sex）、身份证号（Cardid）、联系方式（Tel）、专业（Major）、年级（Grade）
        //private int id;
        //private string name;
        //private string sex;
        //private string cardid;
        //private string tel;
        private string major;
        private string grade;

        //public int Id { get => id; set => id = value; }
        //public string Name { get => name; set => name = value; }
        //public string Sex { get => sex; set => sex = value; }
        //public string Cardid { get => cardid; set => cardid = value; }
        //public string Tel { get => tel; set => tel = value; }
        public string Major { get => major; set => major = value; }
        public string Grade { get => grade; set => grade = value; }


        public Student()
        {

        }
        public new void Print()
        {
            base.Print();
        }
        public Student(int id, string name, string sex, string cardid, string tel, string major, string grade) : base(id, name, sex, cardid, tel)
        {
            Console.WriteLine("专业:" + major);
            Console.WriteLine("工资号:" + grade);
        }


        //public void Print()
        //{
        //Console.WriteLine("编号:"+ Id);
        //Console.WriteLine("姓名:" + Name);
        //Console.WriteLine("性别:" + Sex);
        //Console.WriteLine("身份证:" + Cardid);
        //Console.WriteLine("电话:" + Tel);
        //    Console.WriteLine("专业:" + Major);
        //    Console.WriteLine("年级:" + Grade);
        //}
    }
}
