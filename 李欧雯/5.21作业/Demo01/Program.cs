﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Student student = new Student(1,"张三", "男", "123456a", "456789", "水管工", "八年级");
            //student.Id = 1;
            //student.Name = "张三";
            //student.Sex = "男";
            //student.Cardid = "123456a";
            //student.Tel = "456789";
            //student.Major = "水管工";
            //student.Grade = "八年级";
            student.Print();

            Student student1 = new Student();
            student1.Id = 2;
            student1.Name = "李四";
            student1.Sex = "女";
            student1.Cardid = "123456b";
            student1.Tel = "296859";
            student1.Major = "水管工";
            student1.Grade = "九年级";
            student1.Print();

            Teacher teacher = new Teacher();
            teacher.Id = 1;
            teacher.Name = "王五";
            teacher.Sex = "男";
            teacher.Cardid = "123456c";
            teacher.Tel = "557842";
            teacher.Title = "校长";
            teacher.Wageno = "87961";
            teacher.Print();

            Teacher teacher1 = new Teacher();
            teacher1.Id = 2;
            teacher1.Name = "刘八";
            teacher1.Sex = "男";
            teacher1.Cardid = "123456d";
            teacher1.Tel = "65879621";
            teacher1.Title = "班主任";
            teacher1.Wageno = "2438521";
            teacher1.Print();

            Console.ReadKey();
        }
    }
}
