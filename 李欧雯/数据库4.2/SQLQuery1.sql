create database student;

--创建订单表
create table orders(
	orderId int identity(1,1) primary key,
	orderdate date not null
);
insert into orders(orderdate)
	values('2008-01-12'),('2008-02-10'),('2008-02-15'),('2008-03-10')
select * from orders


--创建订购项目表
create table orderltem(
	ltemiD int primary key identity(1,1),
	orderId int references orders(orderId),
	itemType nvarchar(10),
	itemName nvarchar(10),
	theNumber int,
	theMoney int
);
insert into orderltem(orderId,itemType,itemName,theNumber,theMoney)
	values(1,'文具','笔',72,2)
	,(1,'文具','尺',10,1)
	,(1,'体育用品','篮球',1,56)
	,(2,'文具','笔',36,2)
	,(2,'文具','固体胶',20,3)
	,(2,'日常用品','透明胶',2,1)
	,(2,'体育用品','羽毛球',20,3)
	,(3,'文具','订书机',20,3)
	,(3,'文具','订书针',10,3)
	,(3,'文具','裁纸刀',5,5)
	,(4,'文具','笔',20,2)
	,(4,'文具','信纸',50,1)
	,(4,'日常用品','毛巾',4,5)
	,(4,'日常用品','透明胶',30,1)
	,(4,'体育用品','羽毛球',20,3);

select * from orderltem

--1.查询所有订单订购的所有物品数量总和
select sum(theNumber) 所有订单订购的所有物品数量总和 from orderltem
--2.查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价
select orderId, sum(theNumber) 所有物品的数量,avg(theMoney) 平均单价 from orderltem
where orderId<3
group by orderId
having avg(theMoney)<10
--3.查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价
select orderId, sum(theNumber) 所有物品数量,avg(theMoney) 平均单价 from orderltem
group by orderId
having avg(theMoney)<10 and sum(theNumber)>50
--查询每种类别的产品分别订购了几次
select itemType,count(*) from orderltem
group by itemType
--查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select itemType, sum(theNumber) 订购总数量,avg(theMoney) 平均单价 from orderltem
group by itemType
having sum(theNumber)>100
--查询每种产品的订购次数，订购总数量和订购的平均单价
select itemType, count(*) 订购次数, sum(theNumber) 订单总数量, avg(theMoney) 订单平均单价 from orderltem
group by itemType