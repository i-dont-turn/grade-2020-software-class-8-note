﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo03
{
    class Program
    {
        private static int num;

        static void Main(string[] args)
        {
            //3.	用户输入三个数，找出最大的数，打印输出。
            Console.WriteLine("请输入第一个数：");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入第二个数：");
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入第三个数：");
            int c = int.Parse(Console.ReadLine());
            //if (a>b && a>c)
            //{
            //    Console.WriteLine("最大值："+a);
            //}
            //else if (b>a && b>c)
            //{
            //    Console.WriteLine("最大值：" + b);
            //}
            //else
            //{
            //    Console.WriteLine("最大值：" + c);
            //}
            int max = a;
            if (b>a)
            {
                max = b;
            }
            if (c>a)
            {
                max = c;
            }
            Console.WriteLine("最大值："+max);
            Console.ReadKey();
        }
    }
}
