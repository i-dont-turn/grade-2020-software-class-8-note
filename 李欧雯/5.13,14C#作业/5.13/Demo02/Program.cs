﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo02
{
    class Program
    {
        static void Main(string[] args)
        {
            //2.	编写一个程序，请用户输入一个四位整数，
            //将用户输入的四位数的千位、百位、十位和个位分别显示出来，
            //如5632，则显示“用户输入的千位为5，百位为6，十位为3，个位为2”
            Console.WriteLine("请输入一个四位数：");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("四位数："+a);
            int qw = a / 1000;
            Console.WriteLine("千位:"+qw);
            int bw = a / 100 % 10;
            Console.WriteLine("百位:"+bw);
            int sw = a % 100 / 10;
            Console.WriteLine("十位:"+sw);
            int gw = a % 10;
            Console.WriteLine("个位:"+gw);

            Console.ReadKey();
        }
    }
}
