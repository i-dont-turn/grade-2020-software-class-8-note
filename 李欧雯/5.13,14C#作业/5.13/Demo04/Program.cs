﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo04
{
    class Program
    {
        static void Main(string[] args)
        {
            //4.	接受用户输入一个字符；然后判断这个字母是否为元音字母（不区分大小写），
            //元音字母为A、E、I、O、U。用switch case实现；
            Console.WriteLine("请输入一个字母：");
            string str = Console.ReadLine();
            Console.WriteLine("字母："+str);
            switch (str)
            {
                case "a":
                case "A":
                case"e":
                case "E":
                case "i":
                case "I":
                case "o":
                case "0":
                case "u":
                case "U":
                    Console.WriteLine(str+"为元音字母");
                    break;
                default:
                    Console.WriteLine(str + "不为元音字母");
                    break;
                    
            }
            Console.ReadKey();
        }
    }
}
