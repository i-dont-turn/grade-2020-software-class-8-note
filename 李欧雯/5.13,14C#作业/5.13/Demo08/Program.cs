﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo08
{
    class Program
    {
        static void Main(string[] args)
        {
            //8.	实现查找数组元素索引的功能。定义一个数组，然后控制台输入要查找的元素，
            //返回输入值在数组中最后一次出现的位置。若是找不到，请打印找不到。
            int[] a = {1,2,3,2,5};
            Console.WriteLine("请输入一个数字：");
            int num = int.Parse(Console.ReadLine());
            if (Array.IndexOf(a,num)==-1)
            {
                Console.WriteLine("找不到");
            }
            else
            {
                Array.LastIndexOf(a,num);
                Console.WriteLine("该元素的下标为："+ Array.LastIndexOf(a, num));
            }
            Console.ReadKey();
        }
    }
}
