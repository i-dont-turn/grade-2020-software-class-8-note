﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.	求圆的面积
            const double Π = 3.14;
            Console.WriteLine("请输入圆的半径：");
            int r = int.Parse(Console.ReadLine());
            Console.WriteLine("圆的面积："+Π*r*r);
            Console.ReadKey();
        }
    }
}
