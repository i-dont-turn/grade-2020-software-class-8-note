﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo07
{
    class Program
    {
        static void Main(string[] args)
        {
            //7.	定义一个方法，实现一维数组的排序功能，从大到小排序。
            int[] a = {9,5,2,7,8};
            Array.Sort(a);
            Console.WriteLine("排序后的结果为：");
            Array.Reverse(a);
            foreach (var num in a)
            {
                Console.WriteLine(num);
            }
            Console.ReadKey();
        }
    }
}
