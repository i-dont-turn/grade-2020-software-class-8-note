﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo06
{
    class Program
    {
        static void Main(string[] args)
        {
            //6.	在 Main 方法中创建一个 double 类型的数组，并在该数组中存入 5 名学生的考试成绩，
            //计算总成绩和平均成绩。（要求使用foreach语句实现该功能）
            double [] a= {95,86,78,65,89};
            double sum = 0;
            double pjf = 0;
            foreach (double item in a)
            {
                sum = sum+item;
            }
            Console.WriteLine("总分：" + sum);
            pjf = sum / a.Length;
            Console.WriteLine("平均分：" + pjf);
            Console.ReadKey();
        }
    }
}
