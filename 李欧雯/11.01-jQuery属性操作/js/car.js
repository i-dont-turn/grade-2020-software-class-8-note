$(function () {
    // 点击全选按钮，所有商品的复选框状态会变更
    $('.checkall').click(function () {
        $('.j-checkbox').prop('checked', $(this).prop('checked'));
        $('.checkall').prop('checked', $(this).prop('checked'));
        getSum();
    });

    // 商品复选框选中状态切换，全选状态也会切换
    $('.j-checkbox').click(function () {
        // 判断选中的数量和所有复选框的数量是否一致
        let len1 = $('.j-checkbox').length;
        let len2 = $('.j-checkbox:checked').length;
        if (len1 === len2) {
            $('.checkall').prop('checked', true);
        } else {
            $('.checkall').prop('checked', false);
        }getSum();
    });

    // 点击+号，增加数量
    $('.increment').click(function () {
        let num = $(this).siblings('.itxt').val();
        num++;
        $(this).siblings('.itxt').val(num);

        // 修改小计的值
        let price = $(this).closest('.p-num').siblings('.p-price').text();
        let price2 = price.substr(1, price.length - 1);
        let sum = num * Number(price2);
        $(this).closest('.p-num').siblings('.p-sum').text('￥' + sum.toFixed(2));
        getSum();
    });

    $('.decrement').click(function () {
        let num = $(this).siblings('.itxt').val();
        if (num > 1) {
            num--;
            $(this).siblings('.itxt').val(num);

            let price = $(this).closest('.p-num').siblings('.p-price').text();
            let price2 = price.substr(1, price.length - 1);
            let sum = num * Number(price2);
            $(this).closest('.p-num').siblings('.p-sum').text('￥' + sum.toFixed(2));
        }getSum();
    });

    $('.itxt').on('input', function () {
        let num2 = $(this).val();
        // console.log(num2);
        let price3 = $(this).closest('.p-num').siblings('.p-price').text();
        // console.log(price3);
        let price4 = price3.substr(1, price3.length);
        // console.log(price4);
        let sum2 = num2 * Number(price4);
        console.log(sum2);
        $(this).closest('.p-num').siblings('.p-sum').text('￥' + sum2.toFixed(2));
        getSum();
    });

    function getSum(){
        let count = 0;
        let money = 0;
        //对输入框进行循环
        $.each($('.itxt'),function(index,ele){
            console.log($(ele));
            //如果默认选中则加上输入框里面的value的值
            if($('.j-checkbox')[index].checked){
                count = count + Number($(ele).val());
            }
        })
        let a = $('.amount-sum em').text(count);
        // console.log(a);
        //遍历价格，如果该商品为选中状态价格就加上
        $.each($('.p-sum'),function(index,ele){
            if($('.j-checkbox')[index].checked){
               money = money + parseFloat($(ele).text().substr(1));
            }
        })
        let b = $('.price-sum em').text(money.toFixed(2));
        console.log(b);
    }getSum();
})