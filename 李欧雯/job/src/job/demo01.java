package job;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

public class demo01 {

	public static void main(String[] args) {
		// 1.从键盘接受10个整数，求出其中的最大值和最小值。
		int[] a = new int[3];
		Scanner sc = new Scanner(System.in);
		for (int i = 0; i < a.length; i++) {
			System.out.println("请输入第" + (i + 1) + "个数：");
			a[i] = sc.nextInt();
		}
		System.out.println(Arrays.toString(a));
		int max = a[0];
		int min = a[0];
		for (int i = 0; i < a.length; i++) {
			if (a[i] > max) {
				max = a[i];
			}
			if (a[i] < min) {
				min = a[i];
			}
		}
		System.out.println("最大值：" + max);
		System.out.println("最小值：" + min);
	}

}
