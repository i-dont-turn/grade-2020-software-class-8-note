package job;

import java.util.Arrays;
import java.util.Scanner;

public class demo02 {

	public static void main(String[] args) {
		// 2.输入10个数，保存在一个数组中，在数组中查找某个数字，给出是否找到信息，
		// 如果找到了输出该数在数组中所处的位置，如果找不到输出“找不到”
				int[] a = new int[10];
				Scanner sc = new Scanner(System.in);
				for (int i = 0; i < a.length; i++) {
					System.out.println("请输入第" + (i + 1) + "个数：");
					a[i] = sc.nextInt();
				}
				System.out.println(Arrays.toString(a));
				System.out.println("请输入要查找的数：");
				int j = sc.nextInt();
				boolean b = false;
				for (int i = 0; i < a.length; i++) {
					if (j == a[i]) {
						System.out.println(j + "所在数组中的位置为：" + (i + 1));
						b = true;
					}
				}
				if (b == false) {
					System.out.println("找不到");
				}
			}


	}

