package job;

import java.util.Arrays;
import java.util.Scanner;

public class demo03 {

	public static void main(String[] args) {
		// 3.如果一个数组保存元素是有序的（从大到小），
		// 向这个数组中插入一个数，使得插入后的数组元素仍然保持有序。
		int a[] = new int[5];
		Scanner sc = new Scanner(System.in);
		for (int i = 0; i < a.length - 1; i++) {// 用0来占位置
			System.out.println("请输入第" + (i + 1) + "个数：");
			a[i] = sc.nextInt();
		}
		for (int i = 0; i < a.length; i++) {
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] < a[j]) {
					int temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
		System.out.println(Arrays.toString(a));
		System.out.println("请输入你要插入的数：");
		a[a.length - 1] = sc.nextInt();// 预留一个位置给插入的数
		for (int i = 0; i < a.length; i++) {
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] < a[j]) {
					int temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
		System.out.println(Arrays.toString(a));
	}

}
