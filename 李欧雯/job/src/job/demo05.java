package job;

import java.util.Arrays;
import java.util.Scanner;

public class demo05 {

	public static void main(String[] args) {
		//5.将一个数组中的元素逆序输出，即第一个元素和最后一个元素交换，
		//第二个数与倒数第二元素交换…..，例如：原数组为：9  2  5  7   8，
		//逆序后的数组为：8   7   5  2  9
		int a []=new int [5];
		Scanner sc=new Scanner(System.in);
		for (int i = 0; i < a.length; i++) {
			System.out.println("请输入第"+(i+1)+"个数：");
			a[i]=sc.nextInt();
		}
		System.out.println(Arrays.toString(a));
		for (int i = 0; i < a.length/2; i++) {
			int temp=a[i];
			a[i]=a[a.length-1-i];
			a[a.length-1-i]=temp;
		}System.out.println(Arrays.toString(a));
	}

}
