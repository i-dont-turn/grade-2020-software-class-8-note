﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Staff
    {
        //员工类
        public int staffid;
        public string staffname;
        public string staffsex;
        //部门
        public string staffdept;

        public string staffxl;

        public Staff()
        {
            staffxl = "专科";
        }
        public Staff(int staffid, string staffname, string staffsex, string staffdept,string staffxl)
        {
            this.staffid = staffid;
            this.staffname = staffname;
            this.staffsex = staffsex;
            this.staffdept = staffdept;
            this.staffxl = staffxl;
        }

        public void Print()
        {
            Console.WriteLine("工号："+ this.staffid);
            Console.WriteLine("姓名：" + this.staffname);
            Console.WriteLine("性别：" + this.staffsex);
            Console.WriteLine("部门：" + this.staffdept);
            Console.WriteLine("学校：" + staffxl);
        }
    }
}
