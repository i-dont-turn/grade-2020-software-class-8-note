﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Program
    {
        static void Main(string[] args)
        {
            //定义一个员工类，存放用户的工号、姓名、性别、学历和部门信息；
            Staff staff = new Staff();
            staff.staffid = 1;
            staff.staffname = "张三";
            staff.staffsex = "男";
            staff.staffdept = "财务部";
            staff.Print();

            Staff staff1 = new Staff();
            staff1.staffid = 2;
            staff1.staffname = "李四";
            staff1.staffsex = "男";
            staff1.staffdept = "人事部";
            staff1.staffxl = "高中";
            staff1.Print();

            Console.ReadKey();
        }
    }
}
