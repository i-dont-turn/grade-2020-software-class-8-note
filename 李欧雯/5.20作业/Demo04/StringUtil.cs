﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo04
{
    /// <summary>
    /// 工具类
    /// </summary>
    class StringUtil
    {
        public static bool Empty(string str)
        {
            bool a = true;
            if (str!=null && !str.Equals(""))
            {
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i]!=' ')
                    {
                        a = false;
                        break;
                    }
                }
            }return a;
        }
    }
}
