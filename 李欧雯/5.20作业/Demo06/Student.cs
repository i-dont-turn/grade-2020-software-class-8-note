﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo06
{
    class Student
    {
        private string name;
        private static string teachers;

        public string Name { get => name; set => name = value; }
        public string Teachers { get => teachers; set => teachers = value; }


        public Student()
        {

        }
        public Student(string name)
        {
            this.name = name;
        }

        public void Print()
        {
            Console.WriteLine("大家好，我是"+ name+"俺老师叫"+ teachers);
        }
    }
}
