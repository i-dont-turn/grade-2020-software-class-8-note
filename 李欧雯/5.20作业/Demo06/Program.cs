﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo06
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student();
            student.Name = "孙悟空";
            Student student1 = new Student();
            student1.Name = "猪八戒";
            Student student2 = new Student();
            student2.Name = "沙和尚";
            Student student3 = new Student();
            student3.Name = "白龙马";

           
            student.Teachers = "唐僧";
            student.Print();
            student1.Print();
            student2.Print();
            student3.Print();
            student.Teachers = "嫦娥姐姐";
            student.Print();
            student1.Print();
            student2.Print();
            student3.Print();

            Console.ReadKey();
        }
    }
}
