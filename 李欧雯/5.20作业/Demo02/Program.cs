﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo02
{
    class Program
    {
        static void Main(string[] args)
        {
            //1. 定义一个计算图形面积的类，类中定义2个计算面积的方法（重载，方法名相同），
            //分别计算圆面积和长方形面积两个方法。 提示：计算圆的面积传半径，计算长方形面积传长和宽。
            S s = new S();
            Console.WriteLine("请输入长方形的长：");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入长方形的宽：");
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine("长方形的面积："+S.Mj(a,b));

            Console.WriteLine("请输入圆的半径：");
            double c = double.Parse(Console.ReadLine());
            Console.WriteLine("圆的面积："+S.Mj(c));

            Console.ReadKey();
        }
    }
}
