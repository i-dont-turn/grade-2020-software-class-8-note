﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入两个数：");
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine("两数之和：" + SumUtils.js(a, b));

            Console.WriteLine("请输入两个小数：");
            double a1 = double.Parse(Console.ReadLine());
            double b1 = double.Parse(Console.ReadLine());
            Console.WriteLine("两数之和：" + SumUtils.js(a1, b1));

            Console.WriteLine("请输入两个字符：");
            string a2 = Console.ReadLine();
            string b2 = Console.ReadLine();
            Console.WriteLine("拼接后为：" + SumUtils.js(a2, b2));

            Console.WriteLine("请输入一个数：");
            int a3 = int.Parse(Console.ReadLine());
            Console.WriteLine("和："+SumUtils.js(a3));
            Console.ReadKey();
        }
    }
}
