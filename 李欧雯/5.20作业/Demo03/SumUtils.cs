﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo03
{
     /// <summary>
     /// 计算工具类
     /// </summary>
    class SumUtils
    {
        public static int js(int a,int b)
        {
            return a + b;
        }
        public static double js(double c,double d)
        {
            return c + d;
        }
        public static string js(string e,string f)
        {
            return e + f;
        }
        public static int js(int g)
        {
            int sum = 0;
            for (int i = 1; i <=g; i++)
            {
                sum = sum + i;
            }
            return sum;
        }
    }
}
