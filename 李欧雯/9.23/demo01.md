说下你对原型链的理解（面试高频题）。
js中通过prototype的对象指向父类对象，直到指向object对象为止，形成的一条链条，叫做原型链。绝大多数对象最终都会继承自Object.prototype

js中__proto__和prototype的异同点。
_proto_是隐式原型，prototype是显式原型
每个对象都具有_proto_的属性，每个构造函数都具有一个prototype的方法
每个_proto_属性都指向当前自身构造函数的prototype的方法，prototype同样也带有_proto_的属性

Object.prototype 是什么？Object.__proto__ 是什么？Object.prototype.__proto__ 是什么？
Object.prototype是一个对象，是Object原型的对象，是原型链的最终指向是一个实例原型
Object.__proto__就是构造函数Function的原型对象(Function._proto_)
Object.prototype.__proto__是一个null

Array.prototype 是什么？Array.__proto__ 是什么？Array.prototype.__proto__ 是什么？
Array.prototype是一个Array实例原型
Array.__proto__是Array实例的原型(显示没有定义这个函数)
Array.prototype.__proto__是Object.prototype

Function.prototype 是什么？Function.__proto__ 是什么？Function.prototype.__proto__ 是什么？
Function.prototype是一个Function的实例原型
Function.__proto__也是Function的一个实例原型，==Function.prototype
Function.prototype.__proto__是Object.prototype