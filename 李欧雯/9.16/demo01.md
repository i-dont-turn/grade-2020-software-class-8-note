1. 为什么要使用函数？
封装

2. js中函数有几种声明方式？
1.函数声明：function 函数名(){}
2.匿名函数(函数表达式)：var 变量名 = function(){}
3.function的构造函数：var 变量名 = new Function('x','y','return x * y');
4.自调用函数：(function(){})();
5.箭头函数：var 函数名 = (参数) => 返回值

8. call和apply的区别和作用？
作用：改变this的指向
区别：
call：传递的是参数，将参数列表中的参数一个一个传递进来	call(1,2,3);
apply：传递的是参数数组，将参数一数组的形式传递进来		apply([1,2,3]);