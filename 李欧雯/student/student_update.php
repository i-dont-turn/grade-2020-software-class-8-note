<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/6/24
 * Time: 15:35
 */
// 取到学生id
//var_dump($_GET);
$studentId = $_GET['student_id'] ?? '';

// 查询修改的学生信息
$dsn = "sqlsrv:Server=localhost;Database=Student";
$db = new PDO($dsn, "sa", "123456");

$sql = "select * from Student where StudentId='{$studentId}'";
$result = $db->query($sql);
$studentInfo = $result->fetch(PDO::FETCH_ASSOC);
//var_dump($studentInfo);

$sql = 'select * from Class order by ClassId desc';
$result = $db->query($sql);
$classInfo = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>修改学生信息</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div id="container">
    <form method="post" action="student_update_save.php">
        <table class="update">
            <caption>
                <h3>修改学生信息</h3>
            </caption>
            <tr>
                <td>学生id：</td>
                <td><input type="text" name="student_id" value="<?php echo $studentInfo['StudentId'] ?>" readonly="readonly" /></td>
            </tr>
            <tr>
                <td>学生姓名：</td>
                <td><input type="text" name="student_name" value="<?php echo $studentInfo['StudentName'] ?>" /></td>
            </tr>
            <tr>
                <td>学生性别：</td>
                <td>
                    <input type="radio" <?php if ($studentInfo['StudentSex'] == 1) {
                        echo ' checked="checked" ';
                    } ?> name="student_sex" value="1" /> 男
                    <input type="radio"<?php if ($studentInfo['StudentSex'] == 2) {
                        echo ' checked="checked" ';
                    } ?> name="student_sex" value="2"/> 女
                </td>
            </tr>
            <tr>
                <td>学生生日：</td>
                <td><input type="text" name="student_birthday" value="<?php echo $studentInfo['StudentBirth'] ?>"  /></td>
            </tr>
            <tr>
                <td>学生住址：</td>
                <td><input type="text" name="student_address" value="<?php echo $studentInfo['StudentAddress'] ?>" /></td>
            </tr>
            <tr>
                <td>所属班级：</td>
                <td>
                    <select name="class_id">
                        <?php foreach ($classInfo as $key => $value): ?>
                            <option value="<?php echo $value['ClassId']; ?>"><?php echo $value['ClassName'] ?></option>
                        <?php endforeach; ?>
                    </select>

                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="提交" class="btn" />
                    <input type="reset" value="重置" class="btn" />
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>

