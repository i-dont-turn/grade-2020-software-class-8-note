<?php
// 连接数据库，查询出所有的班级信息
$dsn = "sqlsrv:Server=localhost;Database=Student";
$db = new PDO($dsn, "sa", "123456");

$sql = 'select * from Class order by ClassId desc';
$result = $db->query($sql);
$classList = $result->fetchAll(PDO::FETCH_ASSOC);

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>增加学生</title>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
</head>
<body>
<div id="container">
    <form method="post" action="student_add_save.php">
        <table class="update">
            <caption>
                <h3>增加学生</h3>
            </caption>
            <tr>
                <td>学生姓名：</td>
                <td><input type="text" name="student_name"/></td>
            </tr>
            <tr>
                <td>学生性别：</td>
                <td>
                    <input type="radio" checked="checked" name="student_sex" value="1"/> 男
                    <input type="radio" name="student_sex" value="2"/> 女
                </td>
            </tr>
            <tr>
                <td>学生生日：</td>
                <td><input type="text" name="student_birthday"/></td>
            </tr>
            <tr>
                <td>学生住址：</td>
                <td><input type="text" name="student_address"/></td>
            </tr>
            <tr>
                <td>所属班级</td>
                <td>
                    <select name="class_id">
                        <?php foreach ($classList as $key => $value): ?>
                            <option value="<?php echo $value['ClassId']; ?>"><?php echo $value['ClassName'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="提交" class="btn"/>
                    <input type="reset" value="重置" class="btn"/>
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>
