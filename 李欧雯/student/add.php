<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/6/24
 * Time: 15:24
 */
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>增加班级</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div id="container">
    <form method="post" action="add_save.php">
        <table class="update">
            <caption>
                <h3>增加班级信息</h3>
            </caption>
            <tr>
                <td>班级名称：</td>
                <td><input type="text" name="class_name" /></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="提交" class="btn" />
                    <input type="reset" value="重置" class="btn" />
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>

