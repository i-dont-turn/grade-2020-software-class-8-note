﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class user
    {
        private string usernum;
        private string username;
        private string userpassword;


        public string Usernum
        {
            get
            {
                return usernum;
            }
            set
            {
                usernum = value;
            }
        }
        public string Username
        {
            get
            {
                return username;
            }
            set
            {
                username = value;
            }
        }
        public string Userpassword
        {
            get
            {
                return userpassword;
            }
            set
            {
                userpassword = value;
            }
        }
        public void Print()
        {
            Console.WriteLine("账号：" + this.usernum);
            Console.WriteLine("用户名：" + this.username);
            Console.WriteLine("密码：" + this.userpassword);
        }
    }
}
