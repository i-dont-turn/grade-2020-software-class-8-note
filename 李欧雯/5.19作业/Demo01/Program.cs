﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.定义一个用户类，存放用户的账号、用户名和密码属性；
            //在用户类中定义一个方法输出当前用户对象的账号、用户名和密码的信息；然后在主方法调用输出；
            user User = new user();
            User.Usernum = "0001";
            User.Username = "abc";
            User.Userpassword = "789456123";
            Console.WriteLine("用户1的信息：");
            User.Print();

            user User1 = new user();
            User1.Usernum = "0002";
            User1.Username = "def";
            User1.Userpassword = "789456123";
            Console.WriteLine("用户2的信息：");
            User1.Print();
            Console.ReadKey();
        }
    }
}
