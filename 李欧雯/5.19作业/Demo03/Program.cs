﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo03
{
    class Program
    {
        static void Main(string[] args)
        {
            //3.定义一个图书类，存放图书的编号、书名、价格、出版社、作者信息；
            //对价格进行赋值限制，小于0价格，赋值为0
            //在图书类中定义一个方法输出图书信息；
            //在主方法实例化对象，赋值并输出
            book books = new book();
            books.id = "0001";
            books.name = "假如给我三天光明";
            books.Price = -5;
            books.pubcom = "清华出版社";
            books.author = "海伦";
            Console.WriteLine("第一本书的信息");
            books.Print();

            book books1 = new book();
            books1.id = "0002";
            books1.name = "白夜行";
            books1.Price = 40;
            books1.pubcom = "清华出版社";
            books1.author = "东野圭吾";
            Console.WriteLine("第二本书的信息");
            books1.Print();

            Console.ReadKey();
        }
    }
}
