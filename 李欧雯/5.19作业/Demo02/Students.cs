﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo02
{
    class Students
    {
        public string id;
        public string name;
        public string sex;
        private int age;
        public string job;


        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if (value>0 && value<128)
                {
                    age = value;
                }
                else
                {
                    Console.WriteLine("年龄过大");
                }
            }
        }
        public void Print()
        {
            Console.WriteLine("学号：" + this.id);
            Console.WriteLine("姓名：" + this.name);
            Console.WriteLine("性别：" + this.sex);
            Console.WriteLine("年龄：" + this.age);
            Console.WriteLine("专业：" + this.job);
        }
    }
}
