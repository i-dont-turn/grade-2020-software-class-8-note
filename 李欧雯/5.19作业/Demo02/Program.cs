﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo02
{
    class Program
    {
        static void Main(string[] args)
        {
            //2.定义一个学生类，存放学生的学号、姓名、性别、年龄、专业信息；
            //对年龄字段进行赋值的安全性设置，如果是非法值（小于0或者大于128岁），该年龄值为0；
            //在学生类中定义一个方法输出学生信息。
            //在主方法实例化对象，赋值并输出
            Students students = new Students();
            students.id = "001";
            students.name = "何鱼骑";
            students.sex = "女";
            students.Age = 80;
            students.job = "水管工";
            Console.WriteLine("学生1的信息");
            students.Print();

            Students students1 = new Students();
            students1.id = "002";
            students1.name = "林弱飞";
            students1.sex = "男";
            students1.Age = 150;
            students1.job = "水管工";
            Console.WriteLine("学生2的信息");
            students1.Print();
            Console.ReadKey();
        }
    }
}
