import java.util.Scanner;

public class BookManage {
	// 用户信息
	static String[][] user = new String[3][4];
	// 书籍信息
	static String[][] book = new String[6][5];
	// 出版
	static String[][] pubCom = new String[5][3];
	// scanner
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		// 用户信息
		user[0][0] = " HP001"; // 部门
		user[0][1] = "ken";// 用户名
		user[0][2] = "123";// 密码
		user[0][3] = "admin";// 角色

		user[1][0] = " HP001"; // 部门
		user[1][1] = "ken";// 用户名
		user[1][2] = "123";// 密码
		user[1][3] = "admin";// 角色

		user[2][0] = "HP003";
		user[2][1] = "guile";
		user[2][2] = "123";
		user[2][3] = "user";

		// 书籍信息
		book[0][0] = "001"; // isbn编码
		book[0][1] = "JAVA高级编程"; // 书籍名称
		book[0][2] = "100"; // 价格
		book[0][3] = "铁道部出版社"; // 出版社
		book[0][4] = "张三"; // 作者

		book[1][0] = "002";
		book[1][1] = "SQLSERVER高级编程";
		book[1][2] = "150";
		book[1][3] = "清华出版社";
		book[1][4] = "李四";

		book[2][0] = "003";
		book[2][1] = "JAVA基础入门";
		book[2][2] = "50";
		book[2][3] = "铁道部出版社";
		book[2][4] = "张三";

		book[3][0] = "004";
		book[3][1] = "ORACLE高级编程";
		book[3][2] = "120";
		book[3][3] = "清华出版社";
		book[3][4] = "王五";

		// 出版社信息
		pubCom[0][0] = "清华出版社";
		pubCom[0][1] = "武汉市XXXX";
		pubCom[0][2] = "aaa";

		pubCom[1][0] = "铁道出版社";
		pubCom[1][1] = "北京市XXXX";
		pubCom[1][2] = "bbb";

		pubCom[2][0] = "邮电出版社";
		pubCom[2][1] = "南京市XXXX";
		pubCom[2][2] = "ccc";

		boolean flag = true;
		while (flag) {
			System.out.println("***********欢迎使用图书管理系统***********");
			System.out.println("请输入你的用户名：");
			String username = sc.next();
			System.out.println("请输入你的密码：");
			String userpassword = sc.next();
			boolean find = false;
			for (int i = 0; i < user.length; i++) {
				if (username.equals(user[i][1]) && userpassword.equals(user[i][2])) {
					find = true;
					break;
				}
			}
			if (find) {
				System.out.println("登录成功");
				while (true) {
					System.out.println("1 图书管理  2 出版社管理  3 退出系统");
					System.out.println("请输入数字选择功能菜单：");
					int b = sc.nextInt();
					if (b == 1) {
						// 图书管理
						bookmanagement();
					} else if (b == 2) {
						// 出版社管理
						publishmanagement();
					} else if (b == 3) {

						// 退出系统
						System.exit(0);
					} else {
						System.out.println("输入的数字不正确，请重新输入：");
					}
				}
			} else {
				System.out.println("该用户不存在或者密码错误！请重新登录！");
			}
		}
	}

	private static void publishmanagement() {
		// 出版社管理
		while (true) {
			System.out.println("请输入：1.增加 2.删除：出版社有关联的图书不能删除 3.更新 4.根据出版社名称查询 5.查询所有出版社6.返回上一级菜单");
			System.out.println("请输入数字选择功能菜单：");
			int d = sc.nextInt();
			if (d == 1) {
				// 增加
				publishAdd();
			} else if (d == 2) {
				// 删除：出版社有关联的图书不能删除
				publishDelete();
			} else if (d == 3) {
				// 更新
				publishUpdate();
			} else if (d == 4) {
				// 根据出版社名称查询
				publishselect();
			} else if (d == 5) {
				// 查询所有出版社
				publishifo();
			} else if (d == 6) {
				// 返回上一级菜单
				break;
			} else {
				System.out.println("输入错误，请重新选择");
			}
		}
	}

	private static void publishselect() {
		// 根据出版社查询
		System.out.println("请输入你要查询的出版社名字：");
		String publishname=sc.next();
		searchbypn(publishname);
	}

	private static void searchbypn(String publishname) {
		//根据出版社名字查询
		for (int i = 0; i < pubCom.length; i++) {
			if (pubCom[i][0]!=null && pubCom[i][0].indexOf(publishname)!=-1) {
				for (int j = 0; j < pubCom[i].length; j++) {
					System.out.print(pubCom[i][j]+"\t");
				}System.out.println();
			}
		}
	}

	private static void publishUpdate() {
		// 出版社更新
		System.out.println("请输入你要更新的出版社名字：");
		String publishname = sc.next();
		System.out.println("请输入更新后出版社地址：");
		String publishaddress = sc.next();
		System.out.println("请输入更新后的出版社的联系人：");
		String publishpeople = sc.next();
		int gn = getpn(publishname);
		if (gn != -1) {
			pubCom[gn][0] = publishname;
			pubCom[gn][1] = publishaddress;
			pubCom[gn][2] = publishpeople;
			publishifo();
		}else {
			System.out.println("未找到该出版社");
		}
	}

	private static void publishDelete() {
		// 出版社删除
		System.out.println("请输入你要删除的出版社：");
		String publishname = sc.next();
		int gn = getpn(publishname);
		if (gn == -1) {
			System.out.println("没有找到该出版社");
		} else {
			pubCom[gn][0] = null;
			pubCom[gn][1] = null;
			pubCom[gn][2] = null;
			publishifo();
		}
	}

	private static void publishAdd() {
		// 出版社增加
		System.out.println("请输入你要增加的出版社的名称：");
		String publishname = sc.next();
		System.out.println("请输入你要增加的出版社的地址：");
		String publishaddress = sc.next();
		System.out.println("请输入你要增加的出版社的联系人：");
		String publishpeople = sc.next();
		int index = getpn(publishname);
		if (index == -1) {
			int gn = getpnnull();
			if (gn != -1) {
				pubCom[gn][0] = publishname;
				pubCom[gn][1] = publishaddress;
				pubCom[gn][2] = publishpeople;
				System.out.println("添加成功");
				publishifo();
			} else {
				System.out.println("出版社已满，无法添加");
			}
		} else {
			System.out.println("出版社已存在，无法添加");
		}
	}

	private static void publishifo() {
		System.out.println("出版社名称\t出版社地址\t出版社联系人\t");
		for (int i = 0; i < pubCom.length; i++) {
			if (pubCom[i] != null && pubCom[i][0] != null) {
				for (int j = 0; j < pubCom[i].length; j++) {
					System.out.print(pubCom[i][j] + "\t");
				}
				System.out.println();
			}
		}
	}

	private static int getpnnull() {
		int index = -1;
		for (int i = 0; i < pubCom.length; i++) {
			if (pubCom[i][0] == null) {
				index = i;
				break;
			}
		}
		return index;
	}

	private static int getpn(String publishname) {
		int index = -1;
		for (int i = 0; i < pubCom.length; i++) {
			if (publishname.equals(pubCom[i][0])) {
				index = i;
				break;
			}
		}
		return index;
	}

	private static void bookmanagement() {
		// 图书管理
		while (true) {
			System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
			System.out.println("请输入数字选择功能菜单：");
			int c = sc.nextInt();
			if (c == 1) {
				// 增加
				bookAdd();
			} else if (c == 2) {
				// 删除
				bookDelete();
			} else if (c == 3) {
				// 更新
				bookUpdate();
			} else if (c == 4) {
				// 查询
				bookSelect();
			} else if (c == 5) {
				// 返回上一级菜单
				break;
			} else {
				System.out.println("输入错误请重新选择");
			}
		}

	}

	private static void bookSelect() {
		// 图书查询
		while (true) {
			System.out.println("1. 根据ISBN查询\t 2. 根据书名查询（模糊）\t" + "3. 根据出版社查询\t 4. 根据作者查询\t  5. 根据价格范围查询\t"
					+ "\t 6. 查询所有书籍信息\t 7. 返回上一级菜单\t");
			System.out.println("请输入数字选择：");
			int c = sc.nextInt();
			if (c == 1) {
				// 根据ISBN查询
				System.out.println("请输入你要查询的isbn编码：");
				String isbn = sc.next();
				searchbyisbn(isbn);
			} else if (c == 2) {
				// 根据书名查询（模糊）
				System.out.println("请输入你要查询的书名：");
				String bookname = sc.next();
				searchbyname(bookname);
			} else if (c == 3) {
				// 根据出版社查询
				System.out.println("请输入你要查询的出版社：");
				String publish = sc.next();
				searchbypub(publish);
			} else if (c == 4) {
				// 根据作者查询
				System.out.println("请输入你要查询的作者：");
				String author = sc.next();
				searchbyath(author);
			} else if (c == 5) {
				// 根据价格范围查询
				bookbyprice();
			} else if (c == 6) {
				// 查询所有书籍信息
				bookifo();
			} else if (c == 7) {
				// 返回上一级菜单
				break;
			} else {
				System.out.println("输入错误，请重新输入：");
			}
		}

	}

	private static void bookbyprice() {
		// 根据价格查询
		System.out.println("请输入最低价格：");
		double min = sc.nextDouble();
		System.out.println("请输入最高价格：");
		double max = sc.nextDouble();
		for (int i = 0; i < book.length; i++) {
			if (book[i][2] != null) {
				double price = Double.parseDouble(book[i][2]);
				if (price >= min && price <= max) {
					for (int j = 0; j < book[i].length; j++) {
						System.out.print(book[i][j] + "\t");
					}
					System.out.println();
				}
			}
		}
	}

	private static void searchbyath(String author) {
		// 根据作者查询
		for (int i = 0; i < book.length; i++) {
			if (book[i][4] != null && book[i][4].indexOf(author) != -1) {
				for (int j = 0; j < book[i].length; j++) {
					System.out.print(book[i][j] + "\t");
				}
				System.out.println();
			}
		}
	}

	private static void searchbypub(String publish) {
		// 根据出版社查询
		for (int i = 0; i < book.length; i++) {
			if (book[i][3] != null && book[i][3].indexOf(publish) != -1) {
				for (int j = 0; j < book[i].length; j++) {
					System.out.print(book[i][j] + "\t");
				}
				System.out.println();
			}
		}
	}

	private static void searchbyname(String bookname) {
		// 根据书名查询（模糊）
		for (int i = 0; i < book.length; i++) {
			if (book[i][1] != null && book[i][1].indexOf(bookname) != -1) {
				for (int j = 0; j < book[i].length; j++) {
					System.out.println();
					System.out.print(book[i][j] + "\t");
				}
				System.out.println();
			}
		}
	}

	private static void searchbyisbn(String isbn) {
		// 根据ISBN查询
		int index = getbyisbn(isbn);
		System.out.println("书籍isbn \t 书籍名称 \t\t 书籍价格 \t 出版社 \t 作者");
		for (int i = 0; i < book[index].length; i++) {
			System.out.print(book[index][i] + "\t");
		}
		System.out.println();
	}

	private static void bookUpdate() {
		// 图书更新
		System.out.println("请输入你要更新的isbn编码：");
		String isbn = sc.next();
		System.out.println("请输入更新后的图书名称：");
		String bookname = sc.next();
		System.out.println("请输入更新后的图书价格：");
		String price = sc.next();
		System.out.println("请输入更新后的图书出版社：");
		String publish = sc.next();
		System.out.println("请输入更新后的图书作者：");
		String author = sc.next();
		int index = getbyisbn(isbn);
		if (index != -1) {
			save(isbn, bookname, price, publish, author, index);
		} else {
			System.out.println("未找到要修改的isbn的书籍的编码");
		}
		bookifo();
	}

	private static void bookDelete() {
		// 图书删除
		System.out.println("请输入你要删除的书籍的isbn编码：");
		String isbn = sc.next();
		int index = getbyisbn(isbn);
		if (index == -1) {
			System.out.println("没有找到要删除的书籍信息");
		} else {
			book[index][0] = null;
			book[index][1] = null;
			book[index][2] = null;
			book[index][3] = null;
			book[index][4] = null;
			bookifo();
		}

	}

	private static void bookAdd() {
		// 图书增加
		System.out.println("请输入图书的编码：");
		String isbn = sc.next();
		System.out.println("请输入图书的书名：");
		String bookname = sc.next();
		System.out.println("请输入图书的价格：");
		String price = sc.next();
		System.out.println("请输入图书的出版社：");
		String publish = sc.next();
		System.out.println("请输入图书的作者：");
		String author = sc.next();
		// 已存在 无法添加
		int index = getbyisbn(isbn);
		if (index == -1) {
			// 已满 无法添加
			int i = gtenull();
			if (i != -1) {
				save(isbn, bookname, price, publish, author, i);
				System.out.println("添加成功");
				bookifo();
			} else {
				System.out.println("书籍已满，无法添加");
			}
		} else {
			System.out.println("书籍已存在，无法添加");
		}
	}

	private static void bookifo() {
		// 输出书籍信息
		System.out.println("书籍编码\t书籍名称\t\t书籍价格\t出版社\t\t作者");
		for (int i = 0; i < book.length; i++) {
			// 书籍信息和编码不为空
			if (book[i] != null && book[i][0] != null) {
				for (int j = 0; j < book[i].length; j++) {
					System.out.print(book[i][j] + "\t");
				}
				System.out.println();
			}
		}
	}

	private static void save(String isbn, String bookname, String price, String publish, String author, int index) {
		book[index][0] = isbn;
		book[index][1] = bookname;
		book[index][2] = price;
		book[index][3] = publish;
		book[index][4] = author;
	}

	private static int gtenull() {
		int index = -1;
		for (int i = 0; i < book.length; i++) {
			if (book[i][0] == null) {
				index = i;
				break;
			}
		}
		return index;
	}

	private static int getbyisbn(String isbn) {
		int index = -1;
		for (int i = 0; i < book.length; i++) {
			if (isbn.equals(book[i][0])) {
				index = i;
				break;
			}
		}
		return index;
	}

}
