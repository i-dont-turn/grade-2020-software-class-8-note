﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Dog:Animal,Swimming
    {
        public Dog(string name) : base(name)
        {

        }
        public override void Eat()
        {
            Console.WriteLine("{0}吃骨头", name);
        }

        public void Skill()
        {
            Console.WriteLine("{0}会游泳", name);
        }
    }
}
