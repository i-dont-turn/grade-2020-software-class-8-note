﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Duck:Animal,Swimming
    {
        public Duck(string name) : base(name)
        {

        }
        public override void Eat()
        {
            Console.WriteLine("{0}吃草", name);
        }
        public void Skill()
        {
            Console.WriteLine("{0}会游泳", name);
        }
    }
}
