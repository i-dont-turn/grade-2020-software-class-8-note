﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Program
    {
        static void Main(string[] args)
        {
            //猫、狗、鸭、猴，（吃、游泳、爬树）
            Cat cat = new Cat("猫");
            cat.Eat();
            cat.Skill();
            Console.WriteLine("----------------------");
            Dog dog = new Dog("狗");
            dog.Eat();
            dog.Skill();
            Console.WriteLine("----------------------");
            Duck duck = new Duck("鸭");
            duck.Eat();
            duck.Skill();
            Console.WriteLine("----------------------");
            Monkey monkey = new Monkey("猴");
            monkey.Eat();
            monkey.Skill();
            Console.ReadKey();
        }
    }
}
