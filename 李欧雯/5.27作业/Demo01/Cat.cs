﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Cat:Animal,ClimbTree
    {
        public Cat(string name) : base(name)
        {

        }
        public override void Eat()
        {
            Console.WriteLine("{0}吃鱼",name);
        }

        public void Skill()
        {
            Console.WriteLine("{0}会爬树",name);

        }
    }
}
