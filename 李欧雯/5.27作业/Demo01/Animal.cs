﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Animal
    {
        protected string name;
        protected string skill;

        public string Name { get => name; set => name = value; }
        public string Skill { get => skill; set => skill = value; }

        public Animal(string name)
        {
            this.name = name;
        }
        public virtual void Eat()
        {
            
        }

    }
}
