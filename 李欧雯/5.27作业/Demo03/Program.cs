﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo03
{
    class Program
    {
        static void Main(string[] args)
        {

            Superman superman = new Superman();
            superman.Eat();
            superman.Fly();
            superman.TakeOff();
            superman.Land();
            Console.WriteLine("-------------------");
            Bird bird = new Bird();
            bird.Eat();
            bird.TakeOff();
            bird.Fly();
            bird.Land();
            bird.Layeggs();
            Console.WriteLine("-------------------");
            Plane plane = new Plane();
            plane.TakeOff();
            plane.Fly();
            plane.Land();
            plane.CarryPassanger();
            Console.ReadKey();
        }
    }
}
