﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo03
{
    class Bird:Animal,IFlyable
    {
        //public Bird(string name) : base(name)
        //{

        //}
        public override void Eat()
        {
            Console.WriteLine("会吃虫");
        }
        public void TakeOff()
        {
            Console.WriteLine("会起飞");
        }
        public void Fly()
        {
            Console.WriteLine("用翅膀飞");
        }

        public void Land()
        {
            Console.WriteLine("脚着陆");
        }
        public void Layeggs()
        {
            Console.WriteLine("{0}会下蛋");
        }
    }
}
