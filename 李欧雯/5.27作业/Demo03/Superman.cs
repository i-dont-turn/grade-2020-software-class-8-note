﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo03
{
    class Superman:Animal,IFlyable
    {
        //public Superman(string name) : base(name)
        //{

        //}
        public override void Eat()
        {
            Console.WriteLine("会吃饭");
        }
        public void TakeOff()
        {
            Console.WriteLine("会起飞");
        }
        public void Fly()
        {
            Console.WriteLine("用手会飞");
        }

        public void Land()
        {
            Console.WriteLine("用脚着陆");
        }
    }
}
