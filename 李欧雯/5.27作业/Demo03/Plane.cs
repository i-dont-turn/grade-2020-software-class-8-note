﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo03
{
    class Plane:Vehicle,IFlyable
    {
        //public Plane(string name) : base(name)
        //{

        //}
        public void CarryPassanger()
        {
            Console.WriteLine("可以携带旅客");
        }
        public void TakeOff()
        {
            Console.WriteLine("会起飞");
        }
        public void Fly()
        {
            Console.WriteLine("用双翼飞");
        }

        public void Land()
        {
            Console.WriteLine("轮子着陆");
        }
    }
}
