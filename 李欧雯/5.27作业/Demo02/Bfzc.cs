﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo02
{
    class Bfzc:Car,IFly
    {
        public Bfzc(string brand) : base(brand)
        {

        }
        public override void Run()
        {
            Console.WriteLine("{0}会跑",brand);
        }
        public void fly()
        {
            Console.WriteLine("{0}会飞",brand);
        }
    }
}
