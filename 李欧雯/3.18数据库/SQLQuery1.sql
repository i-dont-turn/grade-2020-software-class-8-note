create table Class(
 ClassId int identity(1,1) not null primary key,
 ClassName nvarchar(50),
);
alter table Class add constraint UNQ_Class_ClassName unique (ClassName)
select * from Class;


create table Student(
 StudentId int primary key identity(1,1) not null,
 StudentName nvarchar(50) not null,
 StudentSex tinyint not null default'3',
 StudentBirth date,
 StudentAddress nvarchar(225) not null default'',
 ClassId int not null,
);
alter table Student add constraint CK_Student_StudentSex Check(StudentSex=1 or StudentSex=2 or StudentSex=3)
alter table Student add constraint DF_Student_ClassId default(0) for ClassId
alter table Student add StudentIdentityCards nvarchar(20) not null default''
select * from Student;


create table Course(
 CourseId int identity(1,1) not null primary key,
 CourseName nvarchar(50) not null,
 CourseCredit tinyint not null default'',
);
alter table Course add constraint CK_Course_CourseCredit check(CourseCredit>0)
alter table Course add constraint UNQ_Course_CourseName unique(CourseName)
select * from Course;


create table ClassCourse(
 ClassCourseId int primary key not null identity(1,1),
 ClassId int not null,
 CourseId int not null,
);
alter table ClassCourse add constraint FK_ClassCourse_CourseId foreign key(CourseId) references Course(CourseId)
alter table ClassCourse add constraint FK_ClassCourse_ClassId foreign key(ClassId) references Class(ClassId)
select * from ClassCourse;


create table Score(
 ScoreId int primary key identity(1,1) not null,
 StudentId int not null,
 CourseId int not null,
 Score int not null,
);
alter table Score add constraint FK_Score_CourseId foreign key(CourseId) references Course(CourseId)
alter table Score add constraint FK_Score_StudentId foreign key(StudentId) references Student(StudentId)
alter table Score add constraint CK_Score_Score check(Score>=0)
--1��Ҫ������0Ϊ������
alter table Score add IsResit tinyint not null default'' 
select * from Score;