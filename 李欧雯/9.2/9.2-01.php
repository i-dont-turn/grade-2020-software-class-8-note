<?php
$searchList = [
    ["name" => "百度", "url" => "https://www.baidu.com", "score" => "70"],
    ["name" => "谷歌", "url" => "https://www.google.com", "score" => "90"],
    ["name" => "360搜索", "url" => "https://www.so.com", "score" => "62"],
    ["name" => "搜搜", "url" => "https://www.soso.com", "score" => "65"],
];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
//边框设为1 单元格之间的间隙设为0
<table border="1" cellspacing="0" cellpadding="">
    <tr>//表头
        <th>序号</th>
        <th>网站名</th>
        <th>url地址</th>
    </tr>
    //开始循环 遍历数组中的元素
    <?php foreach ($searchList as $key => $value) :?>
    <tr>
        <td><?php echo $key?></td>
        <td><?php echo $value['name']?></td>
        <td><?php echo $value['url']?></td>
    </tr>
    //结束循环
    <?php endforeach;?>
</table>
</body>
</html>