<?php
//查询一个字符串是否全部由数字组成
$str1 = "123";
echo preg_match("/^\d$/",$str1);
//查询一个字符串是否全部由字母组成
$str2 = "action123";
echo preg_match("/^[a-zA-Z]{0,}$/",$str2);
//检测一个字符串是邮箱
$str3 = "2836888@qq.com";
echo preg_match("/^\d{0,}\@\w{2}\.\w{3}$/",$str3);
//判断一个字符串是手机号
$str4 = "15959778703";
echo preg_match("/1\d{10}/",$str4);
//密码只包含数字，并且长度在6~15位
$str5 = "1234567";
echo preg_match("/^[0-9]{6,15}$/",$str5);
//密码只包含数字和字母，并且既有数字也有字母，长度在6~15位
$str6 = "1B5Ac67";
echo preg_match("/(?!^\d+$)(?!^[a-zA-Z]+$)^[0-9a-zA-Z]{6,15}$/",$str6);
//密码只包含数字、字母和特殊字符~!@#$%^&*，并且数字、字母、特殊字符至少包含1个，长度在6~15位。
$str7 = "123abc@";
$result1 = preg_match("/[0-9a-zA-Z~!@#$%^&*]{6,15}$/",$str7);
$result2 = preg_match("/[0-9]/",$str7);
$result3 = preg_match("/[a-zA-Z]/",$str7);
$result4 = preg_match("/[~!@#$%^&*]/",$str7);
if ($result1 && $result2 && $result3 && $result4){
    echo "成功";
}else{
    echo "不成功";
}