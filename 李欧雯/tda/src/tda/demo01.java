package tda;

import java.util.Arrays;
import java.util.Scanner;

public class demo01 {

	public static void main(String[] args) {
		//班上有10位学生， 每位学生有3门成绩（HTML、 JAVA、 C++） ， 
		//接收用户输入的学生的成绩进行存储， 并计算每位学生的成绩总分和平均分， 最后以表格的形式输出 
		double [][] score=new double[2][3];
		Scanner sc=new Scanner(System.in);
		for (int i = 0; i < score.length; i++) {
			for (int j = 0; j < score[i].length; j++) {
				System.out.println("请输入第"+(i+1)+"个同学的第"+(j+1)+"门成绩：");
				score[i][j]=sc.nextDouble();
			}
		}System.out.println("HTML"+"\t"+"JAVA"+"\t"+"C++"+"\t"+"总分"+"\t"+"平均分");
		for (int i = 0; i < score.length; i++) {
			double sum=0;
			for (int j = 0; j < score[i].length; j++) {
				sum=sum+score[i][j];
				System.out.print(score[i][j]+"\t");
			}
			System.out.print(sum+"\t");
			System.out.println(sum/score[i].length);
		}
	}

}
