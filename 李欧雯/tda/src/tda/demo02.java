package tda;

import java.util.Scanner;

public class demo02 {

	public static void main(String[] args) {
		// 接收并输出某公司本月的考勤和扣款信息， 假设公司有5个员工，
		// 每个员工的考勤项有上下班忘打卡、 迟到、 早退、 旷工，
		// 其中上下班忘打卡扣款10元/次， 迟到和早退扣款为20元/次， 旷工100元/天
		int[][] a = new int[1][5];
		String[] str = { "员工编号", "忘打卡", "迟到", "早退", "旷工" };
		Scanner sc = new Scanner(System.in);
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				System.out.println("请输入第" + (i + 1) + "个员工的" + str[j]+":");
				a[i][j] = sc.nextInt();
			}
			System.out.println("员工编号\t忘打卡\t迟到\t早退\t矿工\t总罚款(单位：元)");
		}
		for (int i = 0; i < a.length; i++) {
			int sum = 0;
			for (int j = 0; j < a[i].length; j++) {
				System.out.print(a[i][j]+"\t");
				
			}sum = sum + a[i][1] * 10 + a[i][2] * 20 + a[i][3] * 20 + a[i][4] * 100;
			System.out.println(sum);
		}

	}
}
