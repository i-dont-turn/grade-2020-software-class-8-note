1.面向对象中的重写是什么意思？
重写其实就是在子类里的方法和父类里方法的同名

2.面向对象中的多态是什么意思？
同样一个功能的多种实现就是多态。

3.面向对象中还有一个重载，请问重载是什么意思？js可以实现吗？
重载是一个类中定义多个同名的方法，但每个方法具有不同的参数类型和参数个数

Js实现重载1、通过arguments对象，去判断函数的参数个数/2、通过prototype对象实现继承,添加新的属性和方法